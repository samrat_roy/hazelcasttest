package com.samrat.rest.model;

import java.io.Serializable;

import org.springframework.util.DigestUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ClusterData implements Serializable{

	private static final long serialVersionUID = 1134980074978001290L;
	private String name;
	private int age;
	private String address;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		return "ClusterData [name=" + name + ", age=" + age + ", address=" + address + "]";
	}

	@JsonIgnore
	public String getMD5Hash() {
		return DigestUtils.md5DigestAsHex(toString().getBytes());
	}
	
}
