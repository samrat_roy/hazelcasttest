package com.samrat.rest.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.hazelcast.config.Config;
import com.hazelcast.config.EntryListenerConfig;
import com.hazelcast.config.ReplicatedMapConfig;
import com.hazelcast.config.YamlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@Configuration
@ComponentScan("com.samrat.hazelcast")
@EntityScan(basePackages = "com.samrat.hazelcast.model")
@EnableJpaRepositories(basePackages = "com.samrat.hazelcast.dao")
public class MainConfig {
	@Value("${application.hazelcast.configlocation}")
	String hazelcastConfiglocation;
	
	@Value("${application.hazelcast.mapname}")
	String hazelcastMapName;
	
	
	@Bean
	public HazelcastInstance hazelcastInstance(HazelCastEntryListner entryListner) throws IOException{
			
		
		YamlConfigBuilder configBuilder=null;

		if(new File(hazelcastConfiglocation).exists()) {
			configBuilder = new YamlConfigBuilder(hazelcastConfiglocation);
		}else {
			throw new FileNotFoundException(String.format("Hazlecast yml configuration not found at '%s'",hazelcastConfiglocation));
		}
		Config config = configBuilder.build();
		config.setProperty("hazelcast.jmx", "true");
		ReplicatedMapConfig mapConfig = config.getReplicatedMapConfig(hazelcastMapName);
		mapConfig.addEntryListenerConfig(new EntryListenerConfig(entryListner, false, true));
		return Hazelcast.newHazelcastInstance(config);
	}

	@Bean
	public CacheManager cacheManager(@Autowired HazelCastEntryListner entryListner) throws IOException{
		return new HazelcastCacheManager(hazelcastInstance(entryListner));
	}
	

}
