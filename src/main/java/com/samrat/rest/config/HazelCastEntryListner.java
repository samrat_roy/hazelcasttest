package com.samrat.rest.config;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.MapEvent;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryEvictedListener;
import com.hazelcast.map.listener.EntryRemovedListener;
import com.hazelcast.map.listener.EntryUpdatedListener;
import com.hazelcast.map.listener.MapClearedListener;
import com.hazelcast.map.listener.MapEvictedListener;
import com.samrat.hazelcast.dao.EventDao;
import com.samrat.hazelcast.model.EventData;
import com.samrat.hazelcast.model.EventLog;

@Component
public class HazelCastEntryListner
		implements EntryAddedListener<String,EventLog>, EntryUpdatedListener<String,EventLog>, EntryRemovedListener<String,EventLog>,
		EntryEvictedListener<String,EventLog>, MapEvictedListener, MapClearedListener {

	// Using an executorService to offload all blocking operations on H2 db in a different thread.
	private ExecutorService hazleCastBlockingOperations = Executors.newCachedThreadPool();
	
	@Autowired
	@Lazy
	private EventDao eventDao;
	
	@Override
	public void mapCleared(MapEvent event) {
		hazleCastBlockingOperations.execute(()->eventDao.deleteAll());
	}

	@Override
	public void mapEvicted(MapEvent event) {
		hazleCastBlockingOperations.execute(()->eventDao.deleteAll());

	}
	

	@Override
	public void entryEvicted(EntryEvent<String, EventLog> event) {
		hazleCastBlockingOperations.execute(()->eventDao.deleteById(event.getKey()));
	}

	@Override
	public void entryRemoved(EntryEvent<String, EventLog> event) {
		hazleCastBlockingOperations.execute(()->eventDao.deleteById(event.getKey()));
		
	}

	@Override
	public void entryUpdated(EntryEvent<String, EventLog> event) {
		hazleCastBlockingOperations.execute(()->updateDB(event));
	}

	@Override
	public void entryAdded(EntryEvent<String, EventLog> event) {
		hazleCastBlockingOperations.execute(()->saveToDB(event));
	}
	
	private void saveToDB(EntryEvent<String, EventLog> event) {
		List<EventData> eventList = event.getValue().getEventList();
		EventLog eventLog = new EventLog(event.getKey(), eventList);
		eventDao.save(eventLog);
	}
	
	private void updateDB(EntryEvent<String, EventLog> event) {
		Optional<EventLog> dbEventLogOptional = eventDao.findById(event.getKey());
		if(dbEventLogOptional.isPresent()) {
			EventLog el  = dbEventLogOptional.get();
			List<EventData> eventList = event.getValue().getEventList();
			EventData lastUpdatedEvent = eventList.get(eventList.size()-1);
			el.getEventList().add(lastUpdatedEvent);
			eventDao.save(el);
		}else {
			EventLog el  = event.getValue();
			eventDao.save(el);
		}
	}

}
