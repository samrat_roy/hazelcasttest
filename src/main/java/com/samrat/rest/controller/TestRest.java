package com.samrat.rest.controller;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.samrat.hazelcast.model.EventLog;
import com.samrat.hazelcast.service.HazlecastCacheService;
import com.samrat.hazelcast.util.MD5HashUtil;
import com.samrat.rest.model.ClusterData;

@RestController("/")
public class TestRest {

	@Autowired
	HazlecastCacheService hzService;
	
	@GetMapping("/cluster")
	public Map<String,EventLog> getCluster() {
		return hzService.getCache();
	}
	
	@PostMapping("/cluster")
	public String addObjectToCluster(@RequestBody ClusterData clusterData) throws IOException{
		String hash = MD5HashUtil.getChecksum(clusterData);
		hzService.updateCache(hash, "addingToCache", clusterData);
		return "added to eventLog " + hzService.getEventsByRequestId(hash);
	}
	
	@PutMapping("/cluster/{id}")
	public String updateObjectToCluster(@PathVariable String id ,@RequestBody ClusterData clusterData) {
		if(hzService.chacheContains(id)) {
			hzService.updateCache(id,"updatingCache",clusterData);
			return "updated eventLog " + hzService.getEventsByRequestId(id); 
		}else {
			return "id not found in cluster "+id;
		}
	}
	
	@DeleteMapping("/cluster/{id}")
	public String deleteById(@PathVariable String id) {
		if(hzService.chacheContains(id)) {
			hzService.removeFromCache(id);
			return "data removed from cluster" ; 
		}
		else {
			return "data not found in cluster";
		}
	}
	
	@DeleteMapping("/cluster")
	public String deleteAll() {
		hzService.clearCache();
		return "cache cleared";
	}
	
	
}
