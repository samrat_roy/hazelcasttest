package com.samrat.hazelcast.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class MD5HashUtil {


	private MD5HashUtil() {	}

	public static String getChecksum(Serializable object) throws IOException {

		try(
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(baos);
				) {
			oos.writeObject(object);
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(baos.toByteArray());
			return DatatypeConverter.printHexBinary(thedigest);
		} catch(NoSuchAlgorithmException md5NoAlgo) {
			throw new RuntimeException("MD5 algorithm not found  ",md5NoAlgo);
		}
	}
}
