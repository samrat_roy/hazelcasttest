package com.samrat.hazelcast.util;

import javax.persistence.AttributeConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonAttributeConverter implements AttributeConverter<Object, String>{

	ObjectMapper objectMapper = new ObjectMapper();
	@Override
	public String convertToDatabaseColumn(Object attribute) {
		
		try {
			return objectMapper.writeValueAsString(attribute);
		} catch (JsonProcessingException e) {
			return "ObjectWriteError" + attribute.toString();
		}
	}

	@Override
	public Object convertToEntityAttribute(String dbData) {
		try {
			return objectMapper.readValue(dbData, Object.class);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

}
