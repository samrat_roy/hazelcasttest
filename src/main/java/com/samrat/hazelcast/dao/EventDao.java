package com.samrat.hazelcast.dao;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.samrat.hazelcast.model.EventLog;

@Repository
public interface EventDao extends CrudRepository<EventLog, String> {
	
	@Query("SELECT el from EventLog el where requestId in :keys")
	Collection<EventLog> loadAll(Collection<String> keys);

	@Query("SELECT el.requestId from EventLog el")
	Collection<String> loadAllKeys();

	@Transactional
	@Modifying
	@Query("DELETE from EventLog where requestId IN :keys")
	void deleteAll(Collection<String> keys);

}
