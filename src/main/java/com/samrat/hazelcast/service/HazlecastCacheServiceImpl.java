package com.samrat.hazelcast.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hazelcast.core.HazelcastInstance;
import com.samrat.hazelcast.model.EventData;
import com.samrat.hazelcast.model.EventLog;

@Service
public class HazlecastCacheServiceImpl implements HazlecastCacheService {

	@Value("${application.hazelcast.mapname}")
	String hazelcastMapName;
	
	@Autowired
	HazelcastInstance hzInstance;
	
	private Map<String,EventLog> hazelcastMap;
	
	@PostConstruct
	private void init() {
		hazelcastMap=hzInstance.getReplicatedMap(hazelcastMapName);
	}
	
	@Override
	public void updateCache(String requestId, String eventName, Object eventData) {
		EventData eventDetails = new EventData(requestId, eventName, eventData);
		if(chacheContains(requestId)) {
			EventLog cacheEventLog = hazelcastMap.get(requestId);
			List<EventData> cacheEventList = cacheEventLog.getEventList();
			cacheEventList.add(eventDetails);
			hazelcastMap.put(requestId,cacheEventLog);
		}else {
			List<EventData> eventList = new ArrayList<>();
			eventList.add(eventDetails);
			hazelcastMap.put(requestId, new EventLog(requestId, eventList));
		}
	}

	@Override
	public void removeFromCache(String requestId) {
		hazelcastMap.remove(requestId);

	}

	@Override
	public EventLog getEventsByRequestId(String requestId) {
		return hazelcastMap.get(requestId);
	}

	@Override
	public boolean chacheContains(String requestId) {
		return hazelcastMap.containsKey(requestId);
	}

	@Override
	public void clearCache() {
		hazelcastMap.clear();
	}

	@Override
	public Map<String, EventLog> getCache() {
		return hazelcastMap;
	}

}
