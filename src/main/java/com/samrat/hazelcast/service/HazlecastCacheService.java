package com.samrat.hazelcast.service;

import java.util.Map;

import com.samrat.hazelcast.model.EventLog;

public interface HazlecastCacheService {

	
	public void removeFromCache(String requestId );
	
	public void updateCache(String requestId , String eventName , Object eventData);
	
	public EventLog getEventsByRequestId(String requestId);
	
	public boolean chacheContains(String requestId);
	
	public void clearCache() ;
	
	public Map<String,EventLog> getCache();
}
