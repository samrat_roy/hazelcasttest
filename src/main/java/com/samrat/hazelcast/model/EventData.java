package com.samrat.hazelcast.model;

import java.io.Serializable;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.samrat.hazelcast.util.JsonAttributeConverter;

@Entity
public class EventData implements Serializable{
	
	private static final long serialVersionUID = 1592601184826132404L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; 
	
	private String requestId;
	
	private String eventName;
	
	@Convert(converter = JsonAttributeConverter.class)
	@Lob
	private Object eventPayload;

	public EventData() {
		super();
	}
	
	public EventData(String requestId, String eventName, Object eventData) {
		super();
		this.requestId = requestId;
		this.eventName = eventName;
		this.eventPayload = eventData;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Object getEventData() {
		return eventPayload;
	}

	public void setEventData(Object eventData) {
		this.eventPayload = eventData;
	}
	
	@Override
	public String toString() {
		return "EventData [id=" + id + ", requestId=" + requestId + ", eventName=" + eventName + ", eventPayload="
				+ eventPayload + "]";
	}
}
