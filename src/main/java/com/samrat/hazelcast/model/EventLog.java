package com.samrat.hazelcast.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class EventLog implements Serializable{

	private static final long serialVersionUID = -4244430791567191659L;

	@Id
	private String requestId;
	
	@OneToMany(mappedBy = "requestId",fetch = FetchType.EAGER , cascade = CascadeType.ALL)
	private List<EventData> eventList;
	
	@UpdateTimestamp
    private LocalDateTime modified;
	
	public EventLog() {
		super();
	}

	public EventLog(String requestId, List<EventData> eventList) {
		super();
		this.requestId = requestId;
		this.eventList = eventList;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public List<EventData> getEventList() {
		return eventList;
	}

	public void setEventList(List<EventData> eventList) {
		this.eventList = eventList;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}

	@Override
	public String toString() {
		return "EventLog [requestId=" + requestId + ", eventList=" + eventList + ", modified=" + modified + "]";
	}

	
}
